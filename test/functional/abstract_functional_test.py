from flask_testing import TestCase

import config
from app.applications.models import Application, InstalledApplication
from app.users.models import User
from app.categories.models import Term, Category
from test.abstract_db_test import AbstractDbTest


class AbstractFunctionalTest(AbstractDbTest, TestCase):

    @classmethod
    def setUpClass(cls):
        config.DISABLE_AUTH = True
        AbstractDbTest.setUpDb()

    def create_app(self):
        return AbstractDbTest.app

    def tearDown(self):
        AbstractDbTest.db.session.rollback()
        AbstractDbTest.db.session.query(Application).delete()
        AbstractDbTest.db.session.query(InstalledApplication).delete()
        AbstractDbTest.db.session.query(User).delete()
        AbstractDbTest.db.session.query(Category).delete()
        AbstractDbTest.db.session.query(Term).delete()
        AbstractDbTest.db.session.commit()


