import config

from app.users.models import User
from test.functional.abstract_functional_test import AbstractFunctionalTest


class UsersApiTest(AbstractFunctionalTest):

    def test_get_all_users(self):
        # given
        config.DISABLE_AUTH = True
        existing_user = User(username="john.doe", name="John Doe", password="==123123==")
        UsersApiTest.db.session.add(existing_user)
        UsersApiTest.db.session.flush()

        # when
        response = self.client.get('/users/')

        # then
        json = response.json
        data = json['data']

        self.assertIsNotNone(data)
        self.assertEqual(1, len(data))
        self.assertEqual('john.doe', data[0]['username'])

    def test_create_update_get_user(self):
        config.DISABLE_AUTH = True
        # create the user
        self.client.put('/users/john.doe', data={
            "name": "John Doe",
            "password": "123"
        })

        response = self.client.get("/users/john.doe")

        json = response.json
        self.assertIsNotNone(json)
        self.assertEqual('John Doe', json['name'])

        # update it
        self.client.put('/users/john.doe', data={
            "name": "John",
            "password": "123"
        })

        response = self.client.get("/users/john.doe")
        json = response.json
        self.assertEqual('John', json['name'])

