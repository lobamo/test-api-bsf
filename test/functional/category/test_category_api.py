from flask import json

from app.applications.models import InstalledApplication, InstalledContent, ContentState
from app.categories.models import Term
from app.users.models import User
from test.functional.abstract_functional_test import AbstractFunctionalTest
from test.unit.tools.test_tools import random_string


class CategoryApiTest(AbstractFunctionalTest):

    def test_terms_api(self):
        term = Term("term1", custom=False)
        CategoryApiTest.db.session.add(term)
        CategoryApiTest.db.session.flush()

        post_response = self.client.post('/terms/', data={'term':'customterm'})
        json = post_response.json
        self.assertEqual('customterm', json['term'])
        self.assertTrue(json['custom'])

        response = self.client.get('/terms/')
        json = response.json
        data = json['data']

        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(data)
        self.assertEqual(2, len(data))
        self.assertEqual("term1", data[0]['term'])
        self.assertEqual(False, data[0]['custom'])
        self.assertEqual("customterm", data[1]['term'])
        self.assertEqual(True, data[1]['custom'])

        response = self.client.delete(data[1]['links'][0]['href'])
        response = self.client.get('/terms/')
        json = response.json
        data = json['data']
        self.assertEqual(1, len(data))
        self.assertEqual("term1", data[0]['term'])

    def test_category(self):

        cat = {
            'labels': {
                'fr': random_string(),
                'en': random_string()
            },
            'tags': ['term1', 'term2']
        }

        post_response = self.client.post('/categories/', data=json.dumps(cat), content_type="application/json")
        self.assertEqual(200, post_response.status_code)

        get_response = self.client.get('/categories/')

        data = get_response.json['data']

        self.assertEqual(1, len(data))
        self.assertEqual(cat['labels']['fr'], data[0]['labels']['fr'])
        self.assertEqual(cat['labels']['en'], data[0]['labels']['en'])

    def test_playlist(self):
        user = User(username="admin", name="Admin", password="123")
        CategoryApiTest.db.session.add(user)
        CategoryApiTest.db.session.flush()

        cat = {
            'title': 'title',
            'user': {
                'url': 'http://localhost/users/admin'
            }
        }

        post_response = self.client.post('/playlists/', data=json.dumps(cat), content_type="application/json")
        self.assertEqual(200, post_response.status_code)

        get_response = self.client.get('/playlists/')

        data = get_response.json['data']

        self.assertEqual(1, len(data))
        self.assertEqual('title', cat['title'])



