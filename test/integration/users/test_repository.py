from app.users import UserRepository
from app.users.models import User, AuthorizationCode
from test.integration.abstract_integration_test import AbstractIntegrationTest


class UserRepositoryTestCase(AbstractIntegrationTest):

    def setUp(self):
        self.user_repository = UserRepositoryTestCase.flask_injector.injector.get(UserRepository)

    def test_get_all(self):
        # given
        user = User(username="john.doe", name="John Doe", password="==123123==")
        UserRepositoryTestCase.db.session.add(user)

        # when
        users = self.user_repository.get_all()

        # then
        self.assertEqual([user], users)

    def test_find_by_username_and_provider(self):
        # given
        john = User(username="john.doe", name="John Doe", password="==123123==")
        jane = User(username="jane.doe", name="Jane Doe", password="==123123==")
        UserRepositoryTestCase.db.session.add(john)
        UserRepositoryTestCase.db.session.add(jane)

        # when
        res = self.user_repository.find_user_by_username_and_provider("jane.doe")

        # then
        self.assertEqual(jane, res)

    def test_find_by_username_and_provider_with_provider(self):
        # given
        john = User(username="john.doe", name="John Doe", password="==123123==")
        jane = User(username="jane.doe", name="Jane Doe", provider="test.app")
        UserRepositoryTestCase.db.session.add(john)
        UserRepositoryTestCase.db.session.add(jane)

        # when
        res = self.user_repository.find_user_by_username_and_provider("jane.doe", provider="test.app")

        # then
        self.assertEqual(jane, res)

    def test_find_user_by_user_id(self):
        # given
        john = User(id=2, username="john.doe", name="John Doe", password="==123123==")
        jane = User(id=3, username="jane.doe", name="Jane Doe", password="==123123==")
        UserRepositoryTestCase.db.session.add(john)
        UserRepositoryTestCase.db.session.add(jane)

        # when
        res = self.user_repository.find_user_by_user_id(3)

        # then
        self.assertEqual(jane, res)

    def test_authorization_code_by_code_and_client_id(self):
        john = User(username="john.doe", name="John Doe", password="==123123==")
        a1 = AuthorizationCode(user=john, code="abcde", client_id="test.app")
        a2 = AuthorizationCode(user=john, code="fghi", client_id="test.app.2")
        UserRepositoryTestCase.db.session.add(a1)
        UserRepositoryTestCase.db.session.add(a2)

        # when
        res = self.user_repository.authorization_code_by_code_and_client_id("fghi", "test.app.2")

        # then
        self.assertEqual(a2, res)

    def test_count_users(self):
        # given
        john = User(id=2, username="john.doe", name="John Doe", password="==123123==")
        jane = User(id=3, username="jane.doe", name="Jane Doe", password="==123123==")
        UserRepositoryTestCase.db.session.add(john)
        UserRepositoryTestCase.db.session.add(jane)

        # when
        result = self.user_repository.count_users()

        # then
        self.assertEqual(2, result)
