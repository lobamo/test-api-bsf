import time
import docker
from unittest import TestCase

from docker.errors import NotFound

import config
from app.core.docker_client import DockerClient
import socket


class DockerClientTest(TestCase):

    @staticmethod
    def wait_for_status(container, expected_status):
        counter = 0
        while container.status != expected_status and counter < 5:
            time.sleep(1)
            container.reload()
            counter += 1

        return container.status

    @staticmethod
    def try_remove_container(container):
        try:
            container.stop()
            counter = 0
            while container.status != 'exited' and counter < 5:
                time.sleep(1)
                container.reload()
                counter += 1
        except Exception as e:
            print(e)

        try:
            container.remove()
        except Exception as e:
            print(e)

    def setUp(self):

        self.docker_client = DockerClient()
        self.docker_sdk = docker.from_env()

    def test_pull_fullimage(self):

        image_name = "busybox:latest"

        # when
        self.docker_client.pull(image_name)

        # then

        img = self.docker_sdk.images.get(image_name)

        self.assertIsNotNone(img)

        self.docker_client.rm_image(image_name)

        try:
            self.docker_sdk.images.get(image_name)
            self.fail("Image %s exists".format(image_name))
        except NotFound:
            print("Image has been deleted")

    def test_create_rm_container(self):
        self.docker_client.pull("nginx:latest")

        container = self.docker_client.create_container(image="nginx:latest", name="test.box.nginx", exposed_port={'80/tcp': 29183})

        self.assertIsNotNone(container)
        status = DockerClientTest.wait_for_status(container,'running')
        self.assertEqual("running", status)
        self._assertConnectionSucceed(host="localhost", port=29183)

        self.docker_client.stop("test.box.nginx")
        status = DockerClientTest.wait_for_status(container, 'exited')
        self.assertEqual("exited", status)

        self.docker_client.rm("test.box.nginx")
        self.assertRaises(NotFound,self.docker_sdk.containers.get,"test.box.nginx")

        DockerClientTest.try_remove_container(container)

    def test_reentrant_stop_and_rm(self):
        self.docker_client.stop("not.existing.container")
        self.docker_client.rm("not.existing.container")
        self.docker_client.rm_network("not.existing.network")
        self.docker_client.rm_image("not.existing.image")

    def test_create_get_rm_network(self):

        self.assertFalse(self.docker_client.network_exists("box.test.network"))

        self.docker_client.create_network("box.test.network")

        self.assertTrue(self.docker_client.network_exists("box.test.network"))

        self.docker_client.rm_network("box.test.network")

        for n in self.docker_sdk.networks.list(names=["box.test.network"]):
            n.remove()
            self.fail("Still a box.test.network")

    def tearDown(self):
        try:
            self.docker_sdk.images.remove("localhost:5000/ciqnxutnaxgi37fpzr2u2gs2l6gbwegxj7a63tu77gw4mwq6hirwkyq/busybox:latest")
        except:
            pass

        self.docker_sdk.close()

    def _assertConnectionSucceed(self, port, host):

        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, port))
            s.close()
        except ConnectionRefusedError as e:
            self.fail("Connection on {} port {} has been refused".format(host, port))







