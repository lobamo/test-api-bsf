import tempfile
from tempfile import TemporaryFile
from unittest import TestCase
import os

from app.core.http_client import HttpClient


class HttpClientTestCase(TestCase):

    def setUp(self) -> None:
        self.httpClient = HttpClient()

    def test_raw_get(self):
        (status_code, content) = self.httpClient.raw_get("http://example.com")

        self.assertEqual(200, status_code)
        self.assertTrue(b"Example Domain" in content)

    def test_get(self):
        (status_code, json) = self.httpClient.get("https://api.github.com/")
        self.assertEqual(200, status_code)
        self.assertTrue("current_user_url" in json)

    def test_get_into(self):

        file = tempfile.gettempdir()+"/example.com"

        self.httpClient.get_into("http://example.com", file)

        try:
            with open(file, "r") as f:
                content = f.read()

                self.assertTrue("Example Domain" in content)
        finally:
            os.remove(file)

    def test_get_into_with_file_like(self):

        file = TemporaryFile()

        self.httpClient.get_into("http://example.com", file)

        file.seek(0)

        content = file.read()

        self.assertTrue(b"Example Domain" in content)

