from app.applications.models import InstalledApplication, InstalledContent, ContentState
from app.categories.repository import CategoryRepository
from app.users.models import User
from test.integration.abstract_integration_test import AbstractIntegrationTest

from app.categories.models import Term, Category, CategoryLabel, Playlist


class CategoryRepositoryTestCase(AbstractIntegrationTest):
    """
    Tests for the categories repository
    """

    def setUp(self):
        self.category_repository = CategoryRepositoryTestCase.flask_injector.injector.get(CategoryRepository)

    def test_delete_all_non_custom_terms(self):
        t1 = Term("term1", custom=False)
        t2 = Term("term2", custom=False)
        t3 = Term("term3", custom=True)

        CategoryRepositoryTestCase.db.session.add(t1)
        CategoryRepositoryTestCase.db.session.add(t2)
        CategoryRepositoryTestCase.db.session.add(t3)

        self.category_repository.delete_all_non_custom_terms()

        all_remaining_terms = CategoryRepositoryTestCase.db.session.query(Term).all()

        self.assertEqual([t3], all_remaining_terms)

    def test_term_save(self):
        term = Term("test", custom=True)

        self.category_repository.save(term)

        all_terms = CategoryRepositoryTestCase.db.session.query(Term).all()

        self.assertEqual(1, len(all_terms))
        self.assertEqual("test", all_terms[0].term)
        self.assertTrue(all_terms[0].custom)

    def test_get_all_terms(self):
        t1 = Term("term1", custom=False)
        t2 = Term("term2", custom=True)

        CategoryRepositoryTestCase.db.session.add(t1)
        CategoryRepositoryTestCase.db.session.add(t2)

        all_terms = self.category_repository.get_all_terms()

        self.assertEqual([t1, t2], all_terms )

    def test_get_all_categories(self):
        c1 = Category()
        c2 = Category()

        CategoryRepositoryTestCase.db.session.add(c1)
        CategoryRepositoryTestCase.db.session.add(c2)

        all_cats = self.category_repository.get_all_categories()

        self.assertEqual([c1, c2], all_cats)

    def test_get_category_by_id(self):
        c1 = Category()
        c2 = Category()

        CategoryRepositoryTestCase.db.session.add(c1)
        CategoryRepositoryTestCase.db.session.add(c2)
        CategoryRepositoryTestCase.db.session.flush()

        cat = self.category_repository.get_category_by_id(c2.id)

        self.assertEqual(c2, cat)

    def test_category_save(self):
        # given
        ia = InstalledApplication(bundle="test.app", target_version="1.0.0")
        ic = InstalledContent(installed_application=ia, content_id="c.id", name="content", size=1,
                              download_path="/down", destination_path="data", current_state=ContentState.installed,
                              target_state=ContentState.installed, target_version="1.0.0")

        CategoryRepositoryTestCase.db.session.add(ia)
        CategoryRepositoryTestCase.db.session.add(ic)
        CategoryRepositoryTestCase.db.session.flush()

        c = Category(tags="term1,term2")
        lab = CategoryLabel(category=c, language="fr", label="Categorie")
        c.installed_applications.append(ia)
        c.installed_contents.append(ic)
        self.category_repository.save(lab)
        self.category_repository.save(c)
        CategoryRepositoryTestCase.db.session.commit()

        # when
        returned_cat = CategoryRepositoryTestCase.db.session.query(Category).first()

        # then
        self.assertEqual(returned_cat, c)
        self.assertEqual([lab], returned_cat.labels)
        self.assertEqual([ia], returned_cat.installed_applications)
        self.assertEqual([ic], returned_cat.installed_contents)

    def test_get_term_by_id(self):
        t1 = Term(id=1, term="term1", custom=False)
        t2 = Term(id=2, term="term2", custom=False)

        CategoryRepositoryTestCase.db.session.add(t1)
        CategoryRepositoryTestCase.db.session.add(t2)
        CategoryRepositoryTestCase.db.session.flush()

        term = self.category_repository.get_term_by_id(1)
        self.assertEqual(t1, term)

    def test_get_all_playlists(self):
        u = User(name="name", username="username", password="123")
        c1 = Playlist(title="title1", user=u)
        c2 = Playlist(title="title2", user=u)

        CategoryRepositoryTestCase.db.session.add(u)
        CategoryRepositoryTestCase.db.session.add(c1)
        CategoryRepositoryTestCase.db.session.add(c2)

        all_cats = self.category_repository.get_all_playlists()

        self.assertEqual([c1, c2], all_cats)

    def test_get_playlist_by_id(self):
        u = User(name="name", username="username", password="123")
        c1 = Playlist(title="title1", user=u)
        c2 = Playlist(title="title2", user=u)

        CategoryRepositoryTestCase.db.session.add(c1)
        CategoryRepositoryTestCase.db.session.add(c2)
        CategoryRepositoryTestCase.db.session.flush()

        cat = self.category_repository.get_playlist_by_id(c2.id)

        self.assertEqual(c2, cat)

    def test_playlist_save(self):
        # given
        u = User(name="name", username="username", password="123")
        ia = InstalledApplication(bundle="test.app", target_version="1.0.0")
        ic = InstalledContent(installed_application=ia, content_id="c.id", name="content", size=1,
                              download_path="/down", destination_path="data", current_state=ContentState.installed,
                              target_state=ContentState.installed, target_version="1.0.0")

        CategoryRepositoryTestCase.db.session.add(ia)
        CategoryRepositoryTestCase.db.session.add(ic)
        CategoryRepositoryTestCase.db.session.flush()

        c = Playlist(title="title1", user=u)
        c.installed_applications.append(ia)
        c.installed_contents.append(ic)
        self.category_repository.save(c)
        CategoryRepositoryTestCase.db.session.commit()

        # when
        returned_cat = CategoryRepositoryTestCase.db.session.query(Playlist).first()

        # then
        self.assertEqual(returned_cat, c)
        self.assertEqual("title1", returned_cat.title)
        self.assertEqual([ia], returned_cat.installed_applications)
        self.assertEqual([ic], returned_cat.installed_contents)
        self.assertEqual(u, returned_cat.user)





