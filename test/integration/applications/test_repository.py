from test.integration.abstract_integration_test import AbstractIntegrationTest

from app.applications.repository import ApplicationRepository
from app.applications.models import Application, InstalledApplication, ApplicationState, InstalledContainer, \
    InstalledContent, ContentState


class ApplicationRepositoryTestCase(AbstractIntegrationTest):
    """
    Test for the application repository
    """

    def setUp(self):
        self.application_repository = ApplicationRepositoryTestCase.flask_injector.injector.get(ApplicationRepository)

    def test_get_all(self):
        # given
        application = Application(name="Test", bundle="test.app", version="1.0.0", description="1.0.0", picture="==picture==")
        ApplicationRepositoryTestCase.db.session.add(application)

        # when
        results = self.application_repository.get_all()

        # then
        expected = Application(name="Test", bundle="test.app", version="1.0.0", description="1.0.0", picture="==picture==")
        expected.id = 1
        self.assertListEqual([expected], results)

    def test_get_all_installed(self):
        # given
        app = InstalledApplication(bundle="test.app", current_state=ApplicationState.downloaded,
                                   target_state=ApplicationState.installed, target_version="1.0.0",
                                   display_weight=2)
        app2 = InstalledApplication(bundle="test.app.2", current_state=ApplicationState.downloaded,
                                   target_state=ApplicationState.installed, target_version="1.0.0",
                                   display_weight=1)

        ApplicationRepositoryTestCase.db.session.add(app)
        ApplicationRepositoryTestCase.db.session.add(app2)

        # when
        results = self.application_repository.get_all_installed()

        # then
        self.assertListEqual([app2, app], results)

    def test_save(self):
        application = Application(name="Test", bundle="test.app", version="1.0.0", description="1.0.0", picture="==picture==")

        self.application_repository.save(application)

        all_apps = ApplicationRepositoryTestCase.db.session.query(Application).all()

        self.assertEqual(1, len(all_apps))

    def test_delete(self):
        # given
        application = Application(name="Test", bundle="test.app", version="1.0.0", description="1.0.0",
                                  picture="==picture==")
        ApplicationRepositoryTestCase.db.session.add(application)
        ApplicationRepositoryTestCase.db.session.flush()

        # when
        results = self.application_repository.delete(application)

        # then
        all_apps = ApplicationRepositoryTestCase.db.session.query(Application).all()
        self.assertEqual(0,len(all_apps))

    def test_find_application_by_bundle(self):
        # given
        session = ApplicationRepositoryTestCase.db.session
        session.add(Application(name="Test", bundle="test.app", version="1.0.0"))
        app2 = Application(name="Test 2", bundle="test.app.2", version="2.0.0")
        session.add(app2)
        ApplicationRepositoryTestCase.db.session.flush()

        # when
        queried_app = self.application_repository.find_application_by_bundle("test.app.2")

        # then
        self.assertEqual(app2, queried_app)

    def test_find_installed_application_by_bundle(self):
        # given
        session = ApplicationRepositoryTestCase.db.session
        installed_application = InstalledApplication(bundle="test.app", target_state=ApplicationState.installed, target_version="1.0.0")
        session.add(installed_application)
        session.flush()

        # when
        result = self.application_repository.find_installed_application_by_bundle("test.app")

        # then
        self.assertEqual(installed_application, result)

    def test_find_installed_app_with_different_state_or_version_state_diff(self):
        # given
        session = ApplicationRepositoryTestCase.db.session
        different_state_application = InstalledApplication(bundle="test.app", current_state=ApplicationState.downloaded,
                                                     target_state=ApplicationState.installed,
                                                     current_version="1.0.0", target_version="1.0.0")
        same_state_application = InstalledApplication(bundle="test.app",
                                                      current_state=ApplicationState.downloaded, target_state=ApplicationState.downloaded,
                                                      current_version="1.0.0", target_version="1.0.0" )
        session.add(different_state_application)
        session.add(same_state_application)
        session.flush()

        # when
        result = self.application_repository.find_installed_app_with_different_state_or_version()

        # then
        self.assertEqual([different_state_application], result)

    def test_find_installed_app_with_different_state_different_version_version_diff(self):
        # given
        session = ApplicationRepositoryTestCase.db.session
        none_current_version_application = InstalledApplication(bundle="test.app", current_state=ApplicationState.installed,
                                                     target_state=ApplicationState.installed, target_version="1.0.0")
        same_version_application = InstalledApplication(bundle="test.app", current_state=ApplicationState.installed,
                                                           target_state=ApplicationState.installed,
                                                           current_version="1.0.0", target_version="1.0.0")
        different_version_application = InstalledApplication(bundle="test.app", current_state=ApplicationState.installed,
                                                        target_state=ApplicationState.installed,
                                                        current_version="1.0.0", target_version="2.0.0")

        session.add(none_current_version_application)
        session.add(same_version_application)
        session.add(different_version_application)
        session.flush()

        # when
        result = self.application_repository.find_installed_app_with_different_state_or_version()

        # then
        self.assertEqual([none_current_version_application, different_version_application], result)

    def test_find_inst_container_by_port_number(self):
        # given
        app = InstalledApplication(bundle="test.app", current_state=ApplicationState.downloaded,
                             target_state=ApplicationState.installed, target_version="1.0.0")
        c1 = InstalledContainer(installed_application=app, name="c1", host_port=10001,
                                original_image="ipfs:image", image="image")
        c2 = InstalledContainer(installed_application=app, name="c2", host_port=10002,
                                original_image="ipfs:image2", image="image2")
        session = ApplicationRepositoryTestCase.db.session
        session.add(app)
        session.add(c1)
        session.add(c2)

        # when
        result = self.application_repository.find_inst_container_by_port_number(10001)

        # then
        self.assertEqual(c1, result)

    def test_find_installed_application_by_current_state(self):
        # given
        app1 = InstalledApplication(bundle="test.app", current_state=ApplicationState.downloaded,
                                    target_state=ApplicationState.downloaded, target_version="1.0.0", display_weight=2)
        app2 = InstalledApplication(bundle="test.app", current_state=ApplicationState.installed,
                                    target_state=ApplicationState.installed, target_version="1.0.0", display_weight=1)
        app3 = InstalledApplication(bundle="test.app", current_state=ApplicationState.downloaded,
                                    target_state=ApplicationState.downloaded, target_version="1.0.0", display_weight=3)
        session = ApplicationRepositoryTestCase.db.session
        session.add(app3)
        session.add(app1)
        session.add(app2)

        # when
        result = self.application_repository.find_installed_application_by_current_state(ApplicationState.downloaded)

        # then
        self.assertEqual([app1, app3], result)

    def test_find_content_with_different_state_and_different_version(self):
        # given
        app = InstalledApplication(bundle="app.id", target_version="1.0.0")
        c1 = InstalledContent(content_id="c3", installed_application=app, current_state=ContentState.uninstalled,
                              target_state=ContentState.installed, name="Content 1", destination_path="/test",
                              download_path="/ipfs/path", current_version="2.0.0", target_version="2.0.0", size=1)
        c2 = InstalledContent(content_id="c4", installed_application=app, current_state=ContentState.installed,
                              target_state=ContentState.installed, name="Content 2", destination_path="/test2",
                              download_path="/ipfs/path2", current_version="2.0.0", target_version="2.0.0", size=1)
        c3 = InstalledContent(content_id="c5", installed_application=app, current_state=ContentState.installed,
                              target_state=ContentState.installed, name="Content 2", destination_path="/test2",
                              download_path="/ipfs/path2", current_version="1.0.0", target_version="2.0.0", size=1)

        session = ApplicationRepositoryTestCase.db.session
        session.add(app)
        session.add(c1)
        session.add(c2)
        session.add(c3)

        # when
        results = self.application_repository.find_content_with_different_state_or_different_version()

        # then
        self.assertEqual([c1, c3], results)

    def test_find_inst_app_with_search_container(self):
        # given
        app = InstalledApplication(bundle="app.id",
                                   target_version="1.0.0",
                                   search_url="/search",
                                   search_container="c1",
                                   current_state=ApplicationState.installed)

        app2 = InstalledApplication(bundle="app.id",
                                    target_version="1.0.0",
                                    current_state=ApplicationState.installed)

        app3 = InstalledApplication(bundle="app.id",
                                   target_version="1.0.0",
                                   search_url="/search",
                                   search_container="c1",
                                   current_state=ApplicationState.downloaded)

        session = ApplicationRepositoryTestCase.db.session
        session.add(app)
        session.add(app2)
        session.add(app3)

        # when
        results = self.application_repository.find_inst_app_with_search_container(ApplicationState.installed)

        # then
        self.assertEqual([app], results)

    def test_find_inst_app_with_auth_source_container(self):
        # given
        app = InstalledApplication(bundle="app.id",
                                   target_version="1.0.0",
                                   auth_source_url="/search",
                                   auth_source_container="c1",
                                   current_state=ApplicationState.installed)

        app2 = InstalledApplication(bundle="app.id",
                                    target_version="1.0.0",
                                    current_state=ApplicationState.installed)

        app3 = InstalledApplication(bundle="app.id",
                                    target_version="1.0.0",
                                    auth_source_url="/search",
                                    auth_source_container="c1",
                                    current_state=ApplicationState.downloaded)

        session = ApplicationRepositoryTestCase.db.session
        session.add(app)
        session.add(app2)
        session.add(app3)

        # when
        results = self.application_repository.find_inst_app_with_auth_source_container(ApplicationState.installed)

        # then
        self.assertEqual([app], results)