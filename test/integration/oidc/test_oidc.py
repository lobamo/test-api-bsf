import os

import flask

import config
import app.core as core_module

from unittest.mock import Mock
from flask_restplus import Api
from authlib.flask.oauth2 import AuthorizationServer
from flask_injector import FlaskInjector
from injector import Module, provider, singleton
from unittest import TestCase
from flask import Flask
from app import Configuration
from app.applications import ApplicationService
from app.applications.models import Application, InstalledApplication
from app.oidc.oidc_endpoints import oauth_api
from app.users import UserService
from app.users.models import User
from app.oidc.oidc import use_oauth


class ServiceMockModule(Module):

    def __init__(self, user_service_mock, authorization_server_mock, application_service_mock):
        self.user_service_mock = user_service_mock
        self.authorization_server_mock = authorization_server_mock
        self.application_service_mock = application_service_mock

    @provider
    @singleton
    def provide_user_service(self) -> UserService:
        return self.user_service_mock

    @provider
    @singleton
    def provide_application_service(self) -> ApplicationService:
        return self.application_service_mock

    @provider
    @singleton
    def provide_authorization_server_mock(self) -> AuthorizationServer:
        return self.authorization_server_mock

    @provider
    @singleton
    def provide_config(self) -> Configuration:
        return config


class BaseOidcTest(TestCase):

    def setUp(self):
        app = Flask(__name__, instance_relative_config=True)
        app.template_folder = os.path.dirname(core_module.__file__) + "/templates"
        # Load the config file
        app.config.from_object(config)

        self.service_mock = Mock(spec=UserService)
        self.authorization_server_mock = Mock(spec=AuthorizationServer)
        self.application_service_mock = Mock(spec=ApplicationService)

        use_oauth(app)
        # Initiate the Flask API
        api = Api(
            title='Box API',
            version='1.0',
            description='Box API',
            # All API metadatas
        )

        # Load application namespace
        api.add_namespace(oauth_api, path="/")
        api.init_app(app)

        modules = [ServiceMockModule(self.service_mock, self.authorization_server_mock, self.application_service_mock)]
        self.injector = FlaskInjector(app=app, modules=modules)

        self.client = app.test_client()


class OidcAuthorizeTest(BaseOidcTest):

    def test_login_page_no_existing_user(self):
        # given
        self.application_service_mock.auth_source_installed_application.return_value = []

        # when
        response = self.client.get("/oauth/authorize")

        self.assertEqual(200, response.status_code)

        self._assert_login_form(response.data)
        self._assert_confirm_checkbox(response.data)

    def test_login_page_with_providers(self):
        """
        Ensure that the login page with providers get the corresponding tabs
        :return:
        """
        app = Application(bundle="test.app", name="Test App", version="1.0.0")
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0")
        self.application_service_mock.auth_source_installed_application.return_value = [ (app, i_app) ]

        # when
        response = self.client.get("/oauth/authorize")

        # then
        data = response.data
        self._assert_tab_button(data, "test.app", "Test App")
        self._assert_tabcontent_tab(data, "test.app")

    def test_grant_page_existing_user_in_session(self):

        with self.client.session_transaction() as sess:
            # given
            sess['username'] = 'jdoe'

        user = User(username="jdoe", name="John Doe", password="1234")
        self.service_mock.get_by_username_and_provider.return_value = user
        self.authorization_server_mock.create_authorization_response.return_value = "RESPONSE", 200

        # when
        response = self.client.get("/oauth/authorize")

        # then
        self.assertEqual(200, response.status_code)
        self.authorization_server_mock.create_authorization_response.assert_called_once_with(grant_user=user)

    def test_authorize_user_login_confirmed(self):
        user = User(username="jdoe", name="John Doe", password="1234")
        self.service_mock.authenticate.return_value = user
        self.authorization_server_mock.create_authorization_response.return_value = "RESPONSE", 200

        # when
        response = self.client.post("/oauth/authorize", data={
            "username": "jdoe",
            "password": "pass",
            "confirm": "true"
        })

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.authenticate.assert_called_once_with("jdoe", "pass", None, "http", "localhost")
        self.authorization_server_mock.create_authorization_response.assert_called_once_with(grant_user=user)
        self.assertEqual("RESPONSE", response.data.decode('utf-8'))

    def test_authorize_user_login_confirmed_with_provider(self):
        user = User(username="jdoe", name="John Doe", provider="test.app")
        self.service_mock.authenticate.return_value = user
        self.authorization_server_mock.create_authorization_response.return_value = "RESPONSE", 200

        # when
        response = self.client.post("/oauth/authorize", data={
            "provider": "test.app",
            "username": "jdoe",
            "password": "pass",
            "confirm": "true"
        })

        # then
        self.assertEqual(200, response.status_code)
        self.service_mock.authenticate.assert_called_once_with("jdoe", "pass", "test.app", "http", "localhost")

    def test_authorize_user_login_failed(self):

        self.service_mock.authenticate.return_value = None
        self.authorization_server_mock.create_authorization_response.return_value = "RESPONSE", 200
        self.application_service_mock.auth_source_installed_application.return_value = []

        # when
        response = self.client.post("/oauth/authorize", data={
            "username": "jdoe",
            "password": "pass",
            "confirm": "true"
        })

        # then
        self.authorization_server_mock.create_authorization_response.assert_not_called()

        self._assert_login_form(response.data)
        self._assert_login_failed_message(response.data)

    def test_authorize_user_login_canceled(self):

        self.service_mock.authenticate.return_value = None
        self.authorization_server_mock.create_authorization_response.return_value = "RESPONSE", 200
        self.application_service_mock.auth_source_installed_application.return_value = []

        # when
        self.client.post("/oauth/authorize", data={
            "cancel": "True"
        })

        # then
        self.authorization_server_mock.create_authorization_response.assert_called_once_with(None)

    def _assert_login_failed_message(self, data):
        data = data.decode('utf-8')
        self.assertTrue('Login failed' in data)

    def _assert_login_form(self, data):
        data = data.decode('utf-8')
        self.assertTrue('<input type="text" name="username"' in data)
        self.assertTrue('<input type="password" name="password"' in data)

    def _assert_confirm_checkbox(self, data):
        data = data.decode('utf-8')
        self.assertTrue('<button type="submit" name="confirm"' in data)

    def _assert_tab_button(self, data, bundle, label):
        data = data.decode('utf-8')

        str = "<button class=\"tablinks\" onclick=\"openProvider('"+bundle+"', event)\">"+label+"</button>"
        self.assertTrue(str in data)

    def _assert_tabcontent_tab(self, data, id):
        data = data.decode('utf-8')

        self.assertTrue('<div id="'+id+'" class="tabcontent">' in data)


class OidcEndpointsTest(BaseOidcTest):

    def test_generate_token(self):
        # given
        self.authorization_server_mock.create_token_response.return_value = "RESPONSE"

        # when
        self.response = self.client.post("/oauth/token", data={})

        # then
        self.authorization_server_mock.create_token_response.assert_called_once_with()

    def test_logoff(self):
        """
        Ensure the session is cleared and the user redirected to the logout url when the
        logout endpoint is reached
        """
        # given
        config.OAUTH2_LOGOUT_REDIRECT_URI = "http://redirect-uri"

        with self.client as c:

            with c.session_transaction() as sess:
                # given
                sess['username'] = 'jdoe'
                sess['provider'] = "kolibri"

            # when
            response = self.client.get('/oauth/logout')

            # then
            self.assertEqual(302, response.status_code)
            self.assertEqual("http://redirect-uri", response.headers['Location'])
            self.assertFalse('username' in flask.session)
            self.assertFalse('provider' in flask.session)

    def test_logoff_whithout_redirect_uri(self):
        """
        Ensure that a redirect uri is calculated by default if none is specified in config when the logout
        endpoint is reached
        """
        # given
        config.API_BASE_URL = "http://my.server.com:8082"
        config.OAUTH2_LOGOUT_REDIRECT_URI = None

        with self.client as c:

            # when
            response = self.client.get('/oauth/logout')

            # then
            self.assertEqual(302, response.status_code)
            self.assertEqual("http://my.server.com", response.headers['Location'])

