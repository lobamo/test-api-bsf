from unittest import TestCase
from unittest.mock import Mock, call

from app.applications import ApplicationRepository
from app.applications.models import InstalledApplication, InstalledContainer, ApplicationState
from app.applications.state_machine.delete_task import DeleteTask
from app.applications.storage.storage import Storage
from app.applications.storage.storage_manager import StorageManager
from app.core.docker_client import DockerClient


class DeleteTaskTest(TestCase):

    def setUp(self):

        self.application_repository_mock = Mock(spec=ApplicationRepository)
        self.docker_client_mock = Mock(spec=DockerClient)

        self.storage_mock = Mock(spec=Storage)
        self.storage_manager_mock= Mock(spec=StorageManager)
        self.storage_manager_mock.storage_for_url.return_value = self.storage_mock
        self.storage_mock.get_protocol.return_value = "ipfs"

        self.delete_task = DeleteTask(self.application_repository_mock,
                                            self.docker_client_mock,
                                            self.storage_manager_mock)

    def test_app_deletion(self):
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0", current_state=ApplicationState.downloaded)
        InstalledContainer(name="c1", installed_application=i_app,
                           original_image="ipfs:ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/img:latest",
                           image="host:port/ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/img:latest")
        self.delete_task.set_bundle("test.app")
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app

        # when
        self.delete_task.run()

        # then
        self.application_repository_mock.find_installed_application_by_bundle.assert_called_once_with("test.app")

        self.docker_client_mock.rm_image.assert_called_once_with(
            "host:port/ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/img:latest")

        self.storage_manager_mock.storage_for_url.assert_called_once_with(
            "ipfs:ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/img:latest"
        )

        self.storage_mock.delete_docker_image.assert_called_once_with(
            "ipfs:ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/img:latest"
        )

        self.assertEqual(ApplicationState.uninstalled, i_app.current_state)
        self.application_repository_mock.commit.assert_called_once_with()

    def test_app_deletion_with_no_storage(self):
        # given
        i_app = InstalledApplication(bundle="test.app", target_version="1.0.0", current_state=ApplicationState.downloaded)
        InstalledContainer(name="c1", installed_application=i_app,
                           original_image="my.server.com:5000/repo/img:latest",
                           image="my.server.com:5000/repo/img:latest")
        self.delete_task.set_bundle("test.app")
        self.application_repository_mock.find_installed_application_by_bundle.return_value = i_app
        self.storage_manager_mock.storage_for_url.return_value = None

        # when
        self.delete_task.run()

        # then
        self.docker_client_mock.rm_image.assert_called_once_with(
            "my.server.com:5000/repo/img:latest"
        )
        self.storage_mock.delete_docker_image.assert_not_called()



