from unittest import TestCase
from unittest.mock import Mock, call

from app.applications import ApplicationRepository
from app.applications.models import Application, Container, InstalledApplication, ApplicationState
from app.applications.state_machine.download_task import DownloadTask
from app.applications.storage.storage import Storage
from app.applications.storage.storage_manager import StorageManager
from app.core.docker_client import DockerClient

import config


class DownloadTaskTest(TestCase):

    def setUp(self):
        self.application_repository = Mock(spec=ApplicationRepository)
        self.docker_client = Mock(spec=DockerClient)

        self.storage_manager_mock = Mock(spec=StorageManager)
        self.storage_mock = Mock(spec=Storage)
        self.storage_mock.get_protocol.return_value = ['ipfs']

        self.download_task = DownloadTask(self.application_repository, self.docker_client,
                                          self.storage_manager_mock, config)

        self.download_task.set_bundle("test.app")

    def test_docker_pull(self):
        # given
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        app.containers = [
            Container(image="ipfs:ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/image:latest", application=app, name="c1"),
            Container(image="ipfs:ciqldqqpo7kdfebfiamv3cofne5i2yhweofbcton4agi36jti5wilea/olip-armhf/kolibri:latest", application=app, name="c2")
        ]

        installed_app = InstalledApplication(bundle="test.app",
                                             current_state=ApplicationState.uninstalled,
                                             target_state=ApplicationState.downloaded,
                                             target_version="1.0.0")

        self.application_repository.find_application_by_bundle.return_value = app
        self.application_repository.find_installed_application_by_bundle.return_value = installed_app

        self.storage_manager_mock.storage_for_url.return_value = self.storage_mock

        # when
        self.download_task.run()

        # then
        self.application_repository.find_application_by_bundle.assert_called_once_with("test.app")
        self.application_repository.find_installed_application_by_bundle.assert_called_once_with("test.app")
        self.docker_client.pull.assert_has_calls([
            call(config.DOCKER_REGISTRY_HOST+"/ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/image:latest"),
            call(config.DOCKER_REGISTRY_HOST+"/ciqldqqpo7kdfebfiamv3cofne5i2yhweofbcton4agi36jti5wilea/olip-armhf/kolibri:latest")
        ])

        # for an unknown reason, we need to specify child mock (on the returned storage) calls has well...
        self.storage_manager_mock.storage_for_url.assert_has_calls([
            call('ipfs:ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/image:latest'),
            call().download_docker_image('ipfs:ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/image:latest'),
            call().get_protocol(),
            call('ipfs:ciqldqqpo7kdfebfiamv3cofne5i2yhweofbcton4agi36jti5wilea/olip-armhf/kolibri:latest'),
            call().download_docker_image('ipfs:ciqldqqpo7kdfebfiamv3cofne5i2yhweofbcton4agi36jti5wilea/olip-armhf/kolibri:latest'),
            call().get_protocol()
        ])

        self.storage_mock.download_docker_image.assert_has_calls([
            call('ipfs:ciqeszuyn5zezqhllxyavi77dowcnq777x62cryolxrodbvmz6l77li/image:latest'),
            call('ipfs:ciqldqqpo7kdfebfiamv3cofne5i2yhweofbcton4agi36jti5wilea/olip-armhf/kolibri:latest')
        ])

        self.assertEqual(ApplicationState.downloaded, installed_app.current_state)

    def test_docker_pull_without_existing_protocol(self):
        """ When a download is triggered for a path not designing an existing protocol, call directly the pull
            on the registry, assuming we have a standard registry """

        # given
        # given
        app = Application(name="Test App", bundle="test.app", version="1.0.0")
        app.containers = [
            Container(image="my.server.com:5000:repo/image:latest", application=app, name="c1"),
        ]

        installed_app = InstalledApplication(bundle="test.app",
                                             current_state=ApplicationState.uninstalled,
                                             target_state=ApplicationState.downloaded,
                                             target_version="1.0.0")

        self.application_repository.find_application_by_bundle.return_value = app
        self.application_repository.find_installed_application_by_bundle.return_value = installed_app

        self.storage_manager_mock.storage_for_url.return_value = None

        # when
        self.download_task.run()

        # then
        self.storage_mock.download_docker_image.assert_not_called()
        self.docker_client.pull.assert_called_once_with('my.server.com:5000:repo/image:latest')


