from unittest import TestCase
from unittest.mock import Mock

from app.applications.storage.local_storage import LocalStorage
from app.core.filesystem import Filesystem


class LocalStorageTest(TestCase):

    def setUp(self):
        self.filesystemMock = Mock(spec=Filesystem)

        self.localStorage = LocalStorage(self.filesystemMock)

    def test_download_descriptor(self):
        """
        Ensure the descriptor is downloaded via http(s) and returned
        """
        # given
        self.filesystemMock.read_fully_as_string.return_value = '{"applications": [{"bundle": "app.id"}]}'

        # when
        descriptor = self.localStorage.download_descriptor("file:///tmp/my/descriptor/folder")

        # then
        self.assertEqual({
            'applications': [{'bundle': 'app.id'}]
        }, descriptor)

        self.filesystemMock.read_fully_as_string.assert_called_once_with("/tmp/my/descriptor/folder/descriptor.json")

    def test_download_descriptor_resource(self):
        """
        Ensure any descriptor resource is downloaded via http relatively to the descriptor root
        :return:
        """
        # given
        self.filesystemMock.read_fully.return_value = b"123123123"

        # when
        res = self.localStorage.download_descriptor_resource("file:///tmp/my/descriptor/folder", "image.png")

        # then
        self.assertEqual(b"123123123", res)

        self.filesystemMock.read_fully.assert_called_once_with("/tmp/my/descriptor/folder/image.png")

    def test_download_content(self):
        # given
        self.localStorage.download_content(
            "file:///tmp/content.zip",
            "/file.zip",
            "/tmp/data/app.id/content",
        )

        # then
        self.filesystemMock.cp.assert_called_once_with("/tmp/content.zip", "/tmp/data/app.id/content/file.zip")

    def test_download_content_with_folders(self):
        self.localStorage.download_content(
            "file:///tmp/content.zip",
            "/an/intermediate/folder/file.zip",
            "/tmp/data/app.id/content"
        )

        self.filesystemMock.makedirs.assert_called_once_with("/tmp/data/app.id/content/an/intermediate/folder")
        self.filesystemMock.cp.assert_called_once_with("/tmp/content.zip",
                                                       "/tmp/data/app.id/content/an/intermediate/folder/file.zip")

    def test_remove_content(self):
        self.localStorage.remove_content(
            "file:///tmp/content.zip",
            "/file.zim",
            "/tmp/data/app.id/content",
        )

        self.filesystemMock.rm.assert_called_once_with('/tmp/data/app.id/content/file.zim')

    def test_remove_content_with_folders(self):
        self.localStorage.remove_content(
            "file:///tmp/content.zip",
            "/an/intermediate/folder/file.zim",
            "/tmp/data/app.id/content",
        )

        self.filesystemMock.removedirs.assert_called_once_with('/tmp/data/app.id/content/an')

    def test_download_zipped_content(self):
        """
        Test that a content is properly unzipped if the download url contains the #unzip anchor
        """
        # when
        self.localStorage.download_content(
            "file:///tmp/content.zip#unzip",
            "/folder",
            "/tmp/data/app.id/content",
        )

        # then
        self.filesystemMock.makedirs.assert_called_once_with("/tmp/data/app.id/content/folder")
        self.filesystemMock.unzipInto.assert_called_once_with(
            "/tmp/content.zip", "/tmp/data/app.id/content/folder")

    def test_remove_unzipped_content(self):
        self.localStorage.remove_content(
            "https://my.example.com/content.zip#unzip",
            "folder",
            "/tmp/data/app.id/content",
        )

        self.filesystemMock.removedirs.assert_called_once_with('/tmp/data/app.id/content/folder')
