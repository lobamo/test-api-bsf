from unittest import TestCase
from unittest.mock import Mock

from app.applications.storage.ipfs_storage import IpfsStorage
from app.core.filesystem import Filesystem
from app.core.ipfs_client import IpfsClient


class IpfsStorageTest(TestCase):

    def setUp(self):
        self.ipfs_client_mock = Mock(spec=IpfsClient)
        self.filesystem_mock = Mock(spec=Filesystem)
        self.ipfs_storage = IpfsStorage(self.ipfs_client_mock, self.filesystem_mock)

    def test_download_descriptor(self):
        # given
        descriptor = {'descriptor':[]}
        self.ipfs_client_mock.get_json_content.return_value=descriptor

        # when
        result = self.ipfs_storage.download_descriptor("ipfs:/ipfs/ABCDEFGH")

        #then
        self.ipfs_client_mock.get_json_content.assert_called_once_with("/ipfs/ABCDEFGH/descriptor.json")
        self.assertEqual(descriptor, result)

    def test_download_descriptor_resource(self):
        #given
        resource = b'12341234'

        self.ipfs_client_mock.cat.return_value = resource

        # when
        result = self.ipfs_storage.download_descriptor_resource("ipfs:/ipfs/ABCDEFGH", "resource.jpg")

        #then
        self.ipfs_client_mock.cat.assert_called_once_with("/ipfs/ABCDEFGH/resource.jpg")
        self.assertEqual(resource, result)

    def test_download_docker_image(self):
        self.ipfs_storage.download_docker_image("ipfs:ciqmcmrhs25dwwmvcqacfqy2hwx5a3r6psa3mg3z22pv5sz4fmhca2q/olip-armhf/kiwix")

        self.ipfs_client_mock.pin.assert_called_once_with("QmbLokak8oRTX7VRjdrGdDSUdSU33hWE3sHj4u1USL3U8h")

    def test_delete_docker_image(self):
        self.ipfs_storage.delete_docker_image("ipfs:ciqmcmrhs25dwwmvcqacfqy2hwx5a3r6psa3mg3z22pv5sz4fmhca2q/olip-armhf/kiwix")

        self.ipfs_client_mock.remove_pin.assert_called_once_with("QmbLokak8oRTX7VRjdrGdDSUdSU33hWE3sHj4u1USL3U8h")

    def test_download_content(self):
        # when
        self.ipfs_storage.download_content(
            "ipfs:/ipfs/QmbLokak8oRTX7VRjdrGdDSUdSU33hWE3sHj4u1USL3U8h",
            "/file.zim",
            "/tmp/data/app.id/content",
        )

        # then
        self.ipfs_client_mock.pin("/ipfs/QmbLokak8oRTX7VRjdrGdDSUdSU33hWE3sHj4u1USL3U8h")

        self.filesystem_mock.symlink.assert_called_once_with(
            "/ipfs/QmbLokak8oRTX7VRjdrGdDSUdSU33hWE3sHj4u1USL3U8h", "/tmp/data/app.id/content/file.zim")

        self.filesystem_mock.makedirs.assert_not_called()

    def test_download_content_with_folders(self):
        self.ipfs_storage.download_content(
            "ipfs:/ipfs/QmbLokak8oRTX7VRjdrGdDSUdSU33hWE3sHj4u1USL3U8h",
            "/an/intermediate/folder/file.zim",
            "/tmp/data/app.id/content",
        )

        self.filesystem_mock.makedirs.assert_called_once_with("/tmp/data/app.id/content/an/intermediate/folder")

    def test_remove_content(self):
        self.ipfs_storage.remove_content(
            "ipfs:/ipfs/QmbLokak8oRTX7VRjdrGdDSUdSU33hWE3sHj4u1USL3U8h",
            "/file.zim",
            "/tmp/data/app.id/content",
        )

        self.ipfs_client_mock.remove_pin.assert_called_once_with('/ipfs/QmbLokak8oRTX7VRjdrGdDSUdSU33hWE3sHj4u1USL3U8h')
        self.filesystem_mock.unlink.assert_called_once_with('/tmp/data/app.id/content/file.zim')
        self.filesystem_mock.removedirs.assert_not_called()

    def test_remove_content_with_folders(self):
        self.ipfs_storage.remove_content(
            "ipfs:/ipfs/QmbLokak8oRTX7VRjdrGdDSUdSU33hWE3sHj4u1USL3U8h",
            "/an/intermediate/folder/file.zim",
            "/tmp/data/app.id/content",
        )

        self.filesystem_mock.removedirs.assert_called_once_with("/tmp/data/app.id/content/an")

