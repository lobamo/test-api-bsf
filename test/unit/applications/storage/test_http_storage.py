from unittest import TestCase
from unittest.mock import Mock, patch, MagicMock

from six import StringIO

from app.applications.storage.http_storage import HttpStorage
from app.core.filesystem import Filesystem
from app.core.http_client import HttpClient


class HttpStorageTest(TestCase):

    def setUp(self):
        self.httpClientMock = Mock(spec=HttpClient)
        self.filesystemMock = Mock(spec=Filesystem)
        self.httpStorage = HttpStorage(self.httpClientMock, self.filesystemMock)

    def test_download_descriptor(self):
        """
        Ensure the descriptor is downloaded via http(s) and returned
        """
        # given
        self.httpClientMock.get.return_value = 200, {"applications": [{"bundle": "app.id"}]}

        # when
        descriptor = self.httpStorage.download_descriptor("http://my.descriptor.com/a/path")

        # then
        self.assertEqual({
            'applications': [{'bundle': 'app.id'}]
        }, descriptor)

        self.httpClientMock.get.assert_called_once_with("http://my.descriptor.com/a/path/descriptor.json")

    def test_download_descriptor_resource(self):
        """
        Ensure any descriptor resource is downloaded via http relatively to the descriptor root
        :return:
        """
        # given
        self.httpClientMock.raw_get.return_value = 200, b"123123123"

        # when
        res = self.httpStorage.download_descriptor_resource("http://my.descriptor.com/a/path", "image.png")

        # then
        self.assertEqual(b"123123123", res)

        self.httpClientMock.raw_get.assert_called_once_with("http://my.descriptor.com/a/path/image.png")

    def test_download_content(self):
        # given
        self.httpStorage.download_content(
            "https://my.example.com/content.zip",
            "/file.zip",
            "/tmp/data/app.id/content",
        )

        # then
        self.httpClientMock.get_into.assert_called_once_with(
            "https://my.example.com/content.zip", "/tmp/data/app.id/content/file.zip")

    def test_download_zipped_content(self):
        """
        Test that a content is properly unzipped if the download url contains the #unzip anchor
        """
        # given
        tempfilemock = MagicMock()
        context_temp_file_mock = Mock()
        tempfilemock.__enter__ = Mock(return_value=context_temp_file_mock)

        self.filesystemMock.tempfile.return_value = tempfilemock

        # when
        self.httpStorage.download_content(
            "https://my.example.com/content.zip#unzip",
            "/folder",
            "/tmp/data/app.id/content",
        )

        # then
        self.filesystemMock.makedirs.assert_called_once_with("/tmp/data/app.id/content/folder")
        self.filesystemMock.tempfile.assert_called_once_with()
        self.httpClientMock.get_into.assert_called_once_with(
            "https://my.example.com/content.zip", context_temp_file_mock)
        context_temp_file_mock.seek.assert_called_once_with(0)
        self.filesystemMock.unzipInto.assert_called_once_with(context_temp_file_mock, "/tmp/data/app.id/content/folder")
        tempfilemock.__enter__.assert_called_once_with()
        tempfilemock.__exit__.assert_called_once_with(None, None, None)

    def test_download_content_with_folders(self):
        self.httpStorage.download_content(
            "https://my.example.com/content.zip",
            "/an/intermediate/folder/file.zip",
            "/tmp/data/app.id/content"
        )

        self.filesystemMock.makedirs.assert_called_once_with("/tmp/data/app.id/content/an/intermediate/folder")

    def test_remove_content(self):
        self.httpStorage.remove_content(
            "https://my.example.com/content.zip",
            "/file.zim",
            "/tmp/data/app.id/content",
        )

        self.filesystemMock.rm.assert_called_once_with('/tmp/data/app.id/content/file.zim')

    def test_remove_content_with_folders(self):
        self.httpStorage.remove_content(
            "https://my.example.com/content.zip",
            "/an/intermediate/folder/file.zim",
            "/tmp/data/app.id/content",
        )

        self.filesystemMock.removedirs.assert_called_once_with('/tmp/data/app.id/content/an')

    def test_remove_unzipped_content(self):
        self.httpStorage.remove_content(
            "https://my.example.com/content.zip#unzip",
            "folder",
            "/tmp/data/app.id/content",
        )

        self.filesystemMock.removedirs.assert_called_once_with('/tmp/data/app.id/content/folder')

