from unittest import TestCase

import config
from app.tools import request_tools, url_tools


class UrlToolsTest(TestCase):

    def test_replace_url_without_internal_url(self):
        """
        Ensure that the method replace_base_url let the given url unchanged if the config parameter
        INTERNAL_HOST_BASE_URL is not defined
        """
        config.INTERNAL_HOST_BASE_URL = None

        self.assertEqual("http://1.2.3.4:8080/test?query_terms=test",
                         url_tools.replace_base_url("http://1.2.3.4:8080/test?query_terms=test"))

    def test_replace_url_with_internal_url(self):
        """
        Ensure that the method replace_base_url replace the base of the given url by the config of the parameter
        INTERNAL_HOST_BASE_URL if defined
        """
        config.INTERNAL_HOST_BASE_URL = "https://root.url:123"

        self.assertEqual("https://root.url:123/test?query_terms=test",
                         url_tools.replace_base_url("http://1.2.3.4:8080/test?query_terms=test"))
