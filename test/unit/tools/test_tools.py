import string
import random


def random_string(n=12):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))

class ArgumentIncluding(dict):
    def __init__(self, key, value):
        self.key = key,
        self.value = value

    def _eq(self, other):
        for key, value in other.items():
            if key == self.key and value == self.value:
                return True

        return False