from unittest import TestCase
from unittest.mock import Mock

from flask import Flask, request

import config
from app.applications import ApplicationService
from app.applications.models import InstalledApplication, InstalledContainer
from app.oidc.oidc import Client


class TestClient(TestCase):

    def setUp(self):
        self.application_service = Mock(spec=ApplicationService)

        self.app = Flask(__name__, instance_relative_config=True)

    def test_client_secret(self):
        # given
        iapp = InstalledApplication(bundle="test.app", client_secret="hashed_password", target_version="1.0.0")
        self.application_service.authenticate_application.return_value = True
        client = Client(self.application_service, iapp)

        # when
        result = client.check_client_secret("password")

        # then
        self.assertTrue(result)
        self.application_service.authenticate_application.assert_called_once_with("test.app", "password")

    def test_client_secret_failure(self):
        # when
        iapp = InstalledApplication(bundle="test.app", client_secret="1234", target_version="1.0.0")
        self.application_service.authenticate_application.return_value = False

        client = Client(self.application_service, iapp)

        # when
        result = client.check_client_secret("password")

        # then
        self.assertFalse(result)

    def test_grant_types(self):
        # when
        iapp = InstalledApplication(bundle="test.app", grant_types="test1,test2,test3", target_version="1.0.0")
        self.application_service.authenticate_application.return_value = False

        client = Client(self.application_service, iapp)

        # when
        self.assertTrue(client.check_grant_type("test2"))
        self.assertFalse(client.check_grant_type("test4"))

    def test_check_redirect_uri(self):
        # when
        config.OAUTH2_ALWAYS_ACCEPT_REDIRECT_URI = False
        iapp = InstalledApplication(bundle="test.app", grant_types="test1,test2,test3", target_version="1.0.0")
        InstalledContainer(name="c1", installed_application=iapp, host_port=10001,
                           original_image="ipfs:image", image="image")
        InstalledContainer(name="c2", installed_application=iapp, host_port=10002,
                           original_image="ipfs:image2", image="image2")
        InstalledContainer(name="c3", installed_application=iapp,
                           original_image="ipfs:image3", image="image3")
        InstalledContainer(name="c4", installed_application=iapp, host_port=10004,
                           original_image="ipfs:image4", image="image4")


        with self.app.test_request_context():
            request.base_url="http://localhost:50002/oauth/authorize"

            client = Client(self.application_service, iapp)

            # then
            self.assertTrue(client.check_redirect_uri("http://localhost:10002"))
            self.assertTrue(client.check_redirect_uri("http://localhost:10004"))
            self.assertFalse(client.check_redirect_uri("http://my.domain:10004"))

    def test_check_token_endpoint_auth_method(self):
        # when
        iapp = InstalledApplication(bundle="test.app", token_endpoint_auth_method="client_secret_basic", target_version="1.0.0")

        client = Client(self.application_service, iapp)

        # then
        self.assertTrue(client.check_token_endpoint_auth_method("client_secret_basic"))
        self.assertFalse(client.check_token_endpoint_auth_method("client_secret_post"))




