from app.users.models import AuthorizationCode, Token
from .models import User
from flask_sqlalchemy import SQLAlchemy
from injector import inject


class UserRepository:

    """Persistence of applications"""
    @inject
    def __init__(self, db: SQLAlchemy):
        self._db = db

    def get_all(self):
        return self._db.session.query(User).all()

    def find_user_by_user_id(self, userid):
        return self._db.session.query(User).filter(User.id == userid).first()

    def find_user_by_username_and_provider(self, username, provider=None):
        return self._db.session.query(User).filter(User.username == username
                                                   and User.provider == provider).first()

    def authorization_code_by_code_and_client_id(self, code, client_id):
        return self._db.session.query(AuthorizationCode).filter(AuthorizationCode.code == code and
                                                                AuthorizationCode.client_id == client_id).first()

    def find_token_by_access_token(self, access_token):
        return self._db.session.query(Token).filter(Token.access_token == access_token).first()

    def count_users(self):
        return self._db.session.query(User).count()

    def save(self, object):
        self._db.session.add(object)

    def flush(self):
        self._db.session.flush()

    def commit(self):
        self._db.session.commit()

    def delete(self, obj):
        self._db.session.delete(obj)