import time

from authlib.specs.oidc import UserInfo
from sqlalchemy import Table, Column, Integer, String, ForeignKey, Text, Boolean
from sqlalchemy.orm import mapper, relationship
from app.core.db import metadata
from authlib.flask.oauth2.sqla import OAuth2AuthorizationCodeMixin

users = Table('users', metadata,
               Column('id', Integer, primary_key=True, autoincrement=True),
               Column('username', String(50), nullable=False, unique=True),
               Column('provider', String(50)),
               Column('name', String(250), nullable=False),
               Column('admin', Boolean),
               Column('password', String(length=50)),
              )

token = Table('tokens', metadata,
              Column('id', Integer, primary_key=True),
              Column('user_id', String(50), ForeignKey('users.id'), nullable=False),
              Column('client_id', String(48)),
              Column('token_type', String(40)),
              Column('access_token', String(255), unique=True, nullable=False),
              Column('refresh_token', String(255), index=True),
              Column('scope', Text(), default=''),
              Column('revoked', Boolean(), default=False),
              Column('issued_at', Integer(), nullable=False, default=lambda: int(time.time())),
              Column('expires_in', Integer(), nullable=False, default=0)
              )

authorization_code = Table('authorization_codes',metadata,
                           Column('id', Integer, primary_key=True),
                           Column('user_id', String(50), ForeignKey('users.id'), nullable=False),
                           Column('code', String(120), unique=True, nullable=False),
                           Column('nonce', String(120)),
                           Column('client_id', String(48)),
                           Column('redirect_uri', Text, default=''),
                           Column('response_type', Text, default=''),
                           Column('scope', Text, default=''),
                           Column('auth_time', Integer, nullable=False, default=lambda: int(time.time()))

                           )


class ExtendedUserInfo(UserInfo):
    def __init__(self):
        super().__init__(self)
        self.username = None
        self.provider = None


class User(object):

    def get_user_id(self):
        """
        Method called by the oidc provider in order fill the sub field
        in the generated token. Consist of the provider plus the user name
        separated by |
        :return:
        """
        sub = self.username
        if self.provider:
            sub = self.provider + "|" + self.username

        return sub

    def generate_user_info(self,scopes) -> UserInfo:
        ui = ExtendedUserInfo()
        ui.sub = self.get_user_id()
        ui.username= self.username

        if self.provider:
            ui.provider = self.provider

        return ui

    def __init__(self, username, name, admin=False, password=None, provider=None, id=None):
        self.id = None
        self.username = username
        self.name = name
        self.admin = admin
        self.password = password
        self.id = id
        self.provider = provider

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id \
                and self.username == other.username \
                and self.name == other.name \
                and self.admin == other.admin \
                and self.password == other.password \
                and self.provider == other.provider

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.username ,self.name, self.admin, self.password, self.provider))


class Token:



    def __init__(self, id=None, user_id=None, client_id=None, token_type=None, access_token=None, refresh_token=None,
                 scope=None, revoked=None, issued_at=None, expires_in=None):
        self.id = id
        self.user_id = user_id
        self.client_id = client_id
        self.token_type = token_type
        self.access_token = access_token
        self.refresh_token = refresh_token
        self.scope = scope
        self.revoked = revoked
        self.issued_at = issued_at
        self.expires_in = expires_in

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id \
                and self.id == other.id \
                and self.user_id == other.user_id \
                and self.client_id == other.client_id \
                and self.token_type == other.token_type \
                and self.access_token == other.access_token \
                and self.refresh_token == other.refresh_token \
                and self.scope == other.scope \
                and self.revoked == other.revoked \
                and self.issued_at == other.issued_at \
                and self.expires_in == other.expires_in

        return eq

    def get_scope(self):
        return self.scope

    def get_expires_in(self):
        return self.expires_in

    def get_expires_at(self):
        return self.issued_at + self.expires_in

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.username))


class AuthorizationCode(OAuth2AuthorizationCodeMixin):

    def __init__(self,id=None, code=None, client_id=None, redirect_uri=None, scope=None, user=None, nonce=None,
                 response_type=None, auth_time=None):
        self.id = id
        self.user = user
        self.code = code
        self.client_id = client_id
        self.redirect_uri = redirect_uri
        self.response_type = response_type
        self.scope = scope
        self.nonce = nonce
        self.auth_time = auth_time

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id \
                and self.id == other.id \
                and self.user == other.user \
                and self.code == other.code \
                and self.client_id == other.client_id \
                and self.redirect_uri == other.redirect_uri \
                and self.response_type == other.response_type \
                and self.scope == other.scope \
                and self.nonce == other.nonce \
                and self.auth_time == other.auth_time

        return eq

    def is_expired(self):
        return self.auth_time + 300 < time.time()

    def get_redirect_uri(self):
        return self.redirect_uri

    def get_scope(self):
        return self.scope

    def get_auth_time(self):
        return self.auth_time

    def get_nonce(self):
        return self.nonce

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.user, self.code, self.client_id, self.redirect_uri, self.response_type, self.scope,
                     self.nonce, self.auth_time))

    def __bytes__(self):
        return self.code.encode('utf-8')


mapper(User, users, properties={})
mapper(Token, token, properties={
    'user': relationship(User)
})
mapper(AuthorizationCode, authorization_code, properties={
    'user': relationship(User)
})

