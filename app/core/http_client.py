import requests


class HttpClient:

    def _request(self, url, auth, headers):
        r = requests.get(url, auth=auth, headers=headers)

        return r

    def raw_get(self, url, auth=None, headers=None):
        r = self._request(url, auth=auth, headers=headers)

        return r.status_code, r.content

    def get(self, url, auth=None, headers=None):
        r = self._request(url, auth=auth, headers=headers)

        return r.status_code, r.json()

    @staticmethod
    def _write_into(r, stream):
        for chunk in r.iter_content(chunk_size=8192):
            if chunk:
                stream.write(chunk)

    def get_into(self, url, path):
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            if hasattr(path, 'read'):
                self._write_into(r, path)
            else:
                with open(path, 'wb') as f:
                    HttpClient._write_into(r, f)
