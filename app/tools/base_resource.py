from authlib.oauth2 import OAuth2Error
from flask_restplus import Resource

import config


class BaseResource(Resource):

    def current_username_and_provider(self, scope=None):
        """
        Returns a tuple containing the username as first entry and the provider as second one
        :return: a tuple (string,string) or None of no currently connecte duser
        """

        if config.DISABLE_AUTH:
            return "admin", None

        try:
            token = self.resource_protector.acquire_token(scope)
        except OAuth2Error as _:
            token = None

        username = provider = None

        if token:
            username = token.user.username
            provider = token.user.provider

        return username, provider

    def current_sub(self):
        """
        Returns the currently connected user under the form of a sub notation (username|provider)
        :return: a string or None if no currently connected user
        """
        (current_username, current_provider) = self.current_username_and_provider()

        current_sub = current_username
        if current_provider:
            current_sub = current_sub+"|"+current_provider

        return current_sub
