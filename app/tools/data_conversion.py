import urllib

from app.tools.request_tools import base_url_from_namespace


def sub_to_username_and_provider(sub):
    """
    Convert a string representation of a username (i.e. provider|user), called subject or sub,
    to a tuple provider,username
    :param sub: the string representation of the provider and user
    :return: a tuble of string provider,username
    """
    provider = None
    username = sub

    if '|' in sub:
        parts = sub.split("|")
        provider = parts[0]
        username = sub[len(provider)+1:]

    return provider, username


def user_to_url_rep(user):
    """
    Convert a user object to its sub representation, i.e. provider|username
    :param user: a user object
    :return: a string
    """
    if user.provider:
        return user.provider+'|'+user.username
    else:
        return user.username


def full_url_to_username_and_provider(url):
    """
    Convert an url that ends with a /users/provider|username path to a tuble provider,username
    :param url: the full url
    :return: a tuple (provider,username)
    """
    url = urllib.parse.urlparse(url)
    path = url.path

    components = path.split('/')

    return sub_to_username_and_provider(components[-1])


def user_to_full_url(namespace,user):

    base_url = base_url_from_namespace(namespace)

    return base_url + '/users/' + user_to_url_rep(user)
