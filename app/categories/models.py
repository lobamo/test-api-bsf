from sqlalchemy import Table, Column, Integer, String, Boolean, Index, ForeignKey, LargeBinary

from app.applications.models import InstalledApplication, InstalledContent
from app.users.models import User, users
from app.core.db import metadata
from sqlalchemy.orm import mapper, relationship

term = Table('terms', metadata,
             Column('id', Integer, primary_key=True, autoincrement=True),
             Column('term', String(20), unique=True, nullable=False),
             Column('custom', Boolean, nullable=False, default=False)
             )

category = Table('categories', metadata,
                 Column('id', Integer, primary_key=True, autoincrement=True),
                 Column('tags', String(250)),
                 Column('thumbnail_mime_type', String(255)),
                 Column('thumbnail', LargeBinary)
                 )

category_labels = Table('categories_labels', metadata,
                        Column('id', Integer, primary_key=True, autoincrement=True),
                        Column('category_id', Integer, ForeignKey('categories.id'), nullable=False),
                        Column('language', String(2), nullable=False),
                        Column('label', String(50), nullable=False)
                        )

category_applications = Table('categories_applications', metadata,
                              Column('category_id', Integer, ForeignKey('categories.id'), nullable=False),
                              Column('installed_application_id', Integer, ForeignKey('installed_applications.id'), nullable=False),
                              )

category_contents = Table('categories_contents', metadata,
                          Column('category_id', Integer, ForeignKey('categories.id'), nullable=False),
                          Column('installed_content_id', Integer, ForeignKey('installed_contents.id'), nullable=False),
                          )

category_playlists = Table('categories_playlists', metadata,
                           Column('category_id', Integer, ForeignKey('categories.id'), nullable=False),
                           Column('playlist_id', Integer, ForeignKey('playlists.id'), nullable=False),
                           )

class Term(object):
    def __init__(self, term, custom=False, id=None):
        self.id = id
        self.term = term
        self.custom = custom

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id \
            and self.term == other.term \
            and self.custom == other.custom

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.term, self.custom))


Index('idx_term_custom', term.c.custom)
mapper(Term, term)


class Category(object):

    def __init__(self, tags=None, id=None):
        self.id = id
        self.tags = tags

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id and \
            self.tags == other.tags

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.tags))


class CategoryLabel(object):
    def __init__(self, category:Category, language, label, id=None):
        self.id = id
        self.category = category
        self.language = language
        self.label = label

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id \
            and self.category == other.category \
            and self.language == other.language \
            and self.label == other.label

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.category, self.language, self.label))


mapper(CategoryLabel, category_labels, properties={
    'category': relationship(Category, back_populates="labels")
})

playlist = Table('playlists', metadata,
                 Column('id', Integer, primary_key=True, autoincrement=True),
                 Column('title', String, nullable=False),
                 Column('user_id', Integer, ForeignKey('users.id'), nullable=False),
                 Column('pinned', Boolean, default=False)
                 )

playlist_applications = Table('playlists_applications', metadata,
                              Column('playlist_id', Integer, ForeignKey('playlists.id'), nullable=False),
                              Column('installed_application_id', Integer, ForeignKey('installed_applications.id'), nullable=False),
                              )

playlist_contents = Table('playlists_contents', metadata,
                          Column('playlist_id', Integer, ForeignKey('playlists.id'), nullable=False),
                          Column('installed_content_id', Integer, ForeignKey('installed_contents.id'), nullable=False),
                          )


class Playlist(object):

    def __init__(self, title, user, pinned=False, id=None):
        self.id = id
        self.title = title
        self.user = user
        self.pinned = pinned

    def __eq__(self, other):
        if other is None:
            return False

        eq = self.id == other.id and \
            self.title == other.title and \
            self.pinned == other.pinned and \
            self.user == other.user

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.id, self.title, self.pinned, self.user))


mapper(Playlist, playlist, properties={
    'user': relationship(User),
    'installed_applications': relationship(InstalledApplication , secondary=playlist_applications),
    'installed_contents': relationship(InstalledContent, secondary=playlist_contents)
})

mapper(Category, category, properties={
    'installed_applications': relationship(InstalledApplication , secondary=category_applications),
    'installed_contents': relationship(InstalledContent, secondary=category_contents),
    'playlists': relationship(Playlist, secondary=category_playlists),
    'labels': relationship(CategoryLabel, back_populates="category", cascade="all, delete-orphan")
})