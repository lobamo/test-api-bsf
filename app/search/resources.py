from urllib.parse import urlparse

from feedgenerator.django.utils import feedgenerator
from flask import Response, make_response, request
from flask_restful import Resource
from flask_restplus import Namespace
from injector import inject

from app.search import SearchService
from app.tools.request_tools import get_hostname

search_api = Namespace('search',
                       description="API exposing global search functions",
                       default_mediatype="application/xml")


@search_api.route("/")
class OpenSearchDescriptorResource(Resource):

    @inject
    def __init__(self, api):
        self.api = api

    def get(self):
        """
        Gets the opensearch descriptor
        """

        base_url = self.api.apis[0].base_url

        xml = ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
               "<OpenSearchDescription xmlns=\"http://a9.com/-/spec/opensearch/1.1/\">"
               "  <ShortName>OLIP</ShortName>"
               "  <Description>OLIP Search Engine</Description>" +
               ("  <Url type=\"application/atom+xml\" template=\"%sopensearch/search?q={searchTerms}\"/>" % base_url) +
               "</OpenSearchDescription>"
               )

        return make_response(Response(xml, headers={'Content-Type': 'application/opensearchdescription+xml'}))


search_parser = search_api.parser()
search_parser.add_argument("q", required=True, help="Term to search")


@search_api.route("/search")
class OpenSearchSearchResource(Resource):

    @inject
    def __init__(self, search_service: SearchService, api):
        self.search_service = search_service
        self.api = api

    @search_api.expect(search_parser)
    def get(self):
        """
        Gets the opensearch descriptor
        """
        args = search_parser.parse_args(request)

        base_url = self.api.apis[0].base_url

        parsed_url = urlparse(base_url)

        results = self.search_service.query_apps(parsed_url.scheme, get_hostname(parsed_url.netloc), args['q'])

        feed = feedgenerator.Atom1Feed(
            title = u"Olip search results",
            link=request.url,
            description = u"Olip search results for query "+args['q'],
        )

        for i in results:
            feed.add_item(i.title, i.url, i.description)

        res = feed.writeString('utf-8')

        return make_response(Response(res, headers={'Content-Type': 'application/atom+xml'}))