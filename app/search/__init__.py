from injector import Module

from app.search.opensearch_client import OpenSearchClient
from app.search.service import SearchService


class SearchModule(Module):
    def configure(self, binder):
        binder.bind(OpenSearchClient, to=OpenSearchClient)
        binder.bind(SearchService, to=SearchService)

