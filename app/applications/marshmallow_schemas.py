from marshmallow import Schema, fields, post_load


# ##################### Configuration schema definition
from app.applications.models import ContainerConfig, ConfigParameter


class ConfigurationValueSchema(Schema):
    name = fields.Str()
    description = fields.Str()
    value = fields.Str()

    @post_load
    def make_container_config(self, data):
        return ConfigParameter(**data)


class ContainerConfigSchema(Schema):
    container = fields.Str()
    parameters = fields.List(fields.Nested(ConfigurationValueSchema))

    @post_load
    def make_container_config(self, data):
        return ContainerConfig(**data)


class ConfigurationRootSchema(Schema):
    configuration = fields.List(fields.Nested(ContainerConfigSchema))
