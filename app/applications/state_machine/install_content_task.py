import logging

from injector import inject

from app.applications.state_machine.base_tasks import BaseContentTask
from app.applications.storage.storage_manager import StorageManager
from app.core.keys import Configuration
from app.applications.repository import ApplicationRepository
from app.applications.models import ContentState

logger = logging.getLogger("InstallContentTask")


class InstallContentTask(BaseContentTask):

    @inject
    def __init__(self, application_repository: ApplicationRepository, config: Configuration,
                 storage_manager: StorageManager):
        super().__init__()
        self.application_repository = application_repository
        self.config = config
        self.bundle = None
        self.storage_manager = storage_manager
        self.bundle_id = None
        self.content_id = None
        self.logger = logger

    def run(self):
        try:
            app = self.application_repository.find_application_by_bundle(self.bundle_id)

            content = next(c for c in app.contents if c.content_id == self.content_id)

            storage = self.storage_manager.storage_for_url(content.download_path)
            storage.download_content(content.download_path,
                                     content.destination_path,
                                     self.get_target_folder(self.config, content))

            # update the status of the content
            self._set_content_to_downloaded(content)

            self.application_repository.commit()
        except:
            self.application_repository.rollback()
            raise

    def _set_content_to_downloaded(self, content):
        ia = self.application_repository.find_installed_application_by_bundle(self.bundle_id)

        ic = next(c for c in ia.installed_contents if c.content_id == self.content_id)

        ic.current_state = ContentState.installed
        ic.current_version = content.version

