import base58
import base64
import os
from threading import Thread

from app.applications.models import ContentState


class BaseTask(Thread):
    def _check_has_content_to_upgrade(self, app, installed_app):

        for i in app.contents:
            for j in installed_app.installed_contents:
                if j.current_state == ContentState.installed and \
                        i.content_id == j.content_id and \
                        i.version != j.current_version:
                    installed_app.has_content_to_upgrade = True
                    return

        installed_app.has_content_to_upgrade = False

    def _image_without_prefix(self, image, storage):
        for p in storage.get_protocol():
            if image.startswith(p+':'):
                return image[len(p)+1:]
        return image

class BaseApplicationTask(BaseTask):

    def __init__(self):
        super().__init__()
        self.bundle = None

    def set_bundle(self, bundle):
        self.bundle = bundle

    @staticmethod
    def image_to_ipfs_hash(image_name):
        parts = image_name.split('/')

        hash = parts[1]

        bytes = base64.b32decode(hash.upper() + '=')
        encoded = base58.b58encode(bytes)

        return encoded.decode('ascii')


class BaseContentTask(BaseTask):

    def __init__(self):
        super().__init__()
        self.bundle_id = None
        self.content_id = None

    def set_content(self, bundle_id, content_id):
        self.bundle_id = bundle_id
        self.content_id = content_id

    def get_target_folder(self, config, content):
        if hasattr(content, 'application'):
            bundle = content.application.bundle
        else:
            bundle = content.installed_application.bundle
        return os.path.join(config.HOST_DATA_DIR, bundle, 'content')
