from threading import Thread

from injector import inject

from app.applications.models import ApplicationState
from app.applications.repository import ApplicationRepository
from app.core.docker_client import DockerClient


class UninstallTask(Thread):

    @inject
    def __init__(self, application_repository: ApplicationRepository, docker_client: DockerClient):
        super().__init__()
        self.application_repository = application_repository
        self.docker_client = docker_client
        self.bundle = None
        self.chained_task = None

    def run(self):

        try:
            installed_app = self.application_repository.find_installed_application_by_bundle(self.bundle)

            for c in installed_app.installed_containers:

                self.docker_client.stop(c.name)
                self.docker_client.rm(c.name)

            self.docker_client.rm_network(self.bundle)

            installed_app.current_state = ApplicationState.downloaded
            installed_app.has_content_to_upgrade = False
            installed_app.current_version = None

            if self.chained_task is None:
                self.application_repository.commit()
            else:
                self.chained_task.set_bundle(self.bundle)
                self.chained_task.run()

        except:
            self.application_repository.rollback()
            raise



    def set_bundle(self, bundle):
        self.bundle = bundle

    def set_chained_task(self, task):
        self.chained_task = task