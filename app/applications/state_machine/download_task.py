from injector import inject

from app.applications.state_machine.base_tasks import BaseApplicationTask
from app.applications.storage.storage_manager import StorageManager
from app.core.keys import Configuration
from app.applications.repository import ApplicationRepository
from app.applications.models import ApplicationState
from app.core.docker_client import DockerClient

import logging

logger = logging.getLogger("DownloadTask")


class DownloadTask(BaseApplicationTask):

    @inject
    def __init__(self, application_repository:ApplicationRepository, docker_client: DockerClient,
                 storage_manager: StorageManager, config: Configuration):
        super().__init__()
        self.application_repository = application_repository
        self.docker_client = docker_client
        self.storage_manager = storage_manager
        self.config = config
        self.bundle = None

    def run(self):
        try:
            application = self.application_repository.find_application_by_bundle(self.bundle)
            installed_application = self.application_repository.find_installed_application_by_bundle(self.bundle)

            for c in application.containers:

                storage = self.storage_manager.storage_for_url(c.image)

                if storage:
                    storage.download_docker_image(c.image)

                    image_without_prefix = self._image_without_prefix(c.image,storage)

                    image = self.config.DOCKER_REGISTRY_HOST + "/" + image_without_prefix
                else:
                    image = c.image

                logger.info("Pulling image %s", image)
                pulled_img = self.docker_client.pull(image)
                logger.debug("Pulled image %s", pulled_img)

            installed_application.current_state=ApplicationState.downloaded

            self.application_repository.commit()
        except:
            self.application_repository.rollback()
            raise


    def set_bundle(self, bundle):
        self.bundle = bundle


