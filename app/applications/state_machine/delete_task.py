from injector import inject

from app.applications.models import ApplicationState
from app.applications.state_machine.base_tasks import BaseApplicationTask
from app.applications.repository import ApplicationRepository
from app.applications.storage.storage_manager import StorageManager
from app.core.docker_client import DockerClient


class DeleteTask(BaseApplicationTask):

    @inject
    def __init__(self, application_repository: ApplicationRepository, docker_client: DockerClient,
                 storage_manager: StorageManager):
        super().__init__()

        self.docker_client = docker_client
        self.application_repository = application_repository
        self.storage_manager = storage_manager

    def run(self):
        try:
            i_app = self.application_repository.find_installed_application_by_bundle(self.bundle)

            for c in i_app.installed_containers:
                self.docker_client.rm_image(c.image)

                storage = self.storage_manager.storage_for_url(c.original_image)

                if storage:
                    storage.delete_docker_image(c.original_image)

            i_app.current_state = ApplicationState.uninstalled

            self.application_repository.commit()
        except:
            self.application_repository.rollback()
            raise



