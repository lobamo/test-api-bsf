from injector import inject

from app.applications.storage import ipfs_storage
from app.applications.storage.http_storage import HttpStorage
from app.applications.storage.ipfs_storage import IpfsStorage

import re

from app.applications.storage.local_storage import LocalStorage


class StorageManager:

    def _register_storage(self, storage):
        prots = storage.get_protocol()

        for c in prots:
            self.storages[c] = storage

    @inject
    def __init__(self, ipfsStorage: IpfsStorage, httpStorage: HttpStorage, localStorage: LocalStorage):

        self.storages = {}

        self._register_storage(ipfsStorage)
        self._register_storage(httpStorage)
        self._register_storage(localStorage)

        self.protocol_matcher = re.compile("^([a-z]*):.*")

    def storage_for_url(self, url):
        result = self.protocol_matcher.search(url)
        if result:
            protocol = result.group(1)
            if protocol in self.storages:
                return self.storages[protocol]
        return None

