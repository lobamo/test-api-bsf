from injector import inject

from app.applications.storage.storage import Storage
from app.core.filesystem import Filesystem
from app.core.ipfs_client import IpfsClient

import base64
import base58
import os
import pathlib


class IpfsStorage (Storage):
    """
    Storage using IPFS as backend
    """

    @inject
    def __init__(self, ipfs_client: IpfsClient, filesystem: Filesystem):
        self.ipfs_client = ipfs_client
        self.filesystem = filesystem

    def _get_target_folder(self, base_folder, target):
        return base_folder + "/" + target

    def _image_to_ipfs_hash(self, image_name):
        # remove the ipfs prefix
        image_name = self.remove_prefix(image_name)

        parts = image_name.split('/')

        hash = parts[0]

        bytes = base64.b32decode(hash.upper() + '=')
        encoded = base58.b58encode(bytes)

        return encoded.decode('ascii')

    def _get_target_full_path(self,base_folder, target):
        target = IpfsStorage._remove_leading_slash_if_any(target)

        return os.path.join(base_folder, target)

    @staticmethod
    def _remove_leading_slash_if_any(path):
        if path[0] == '/':
            path = path[1:]
        return path

    def get_protocol(self):
        return ["ipfs"]

    def download_descriptor(self, path):
        # first a standard get in order to retrieve the whole folder
        real_path = self.remove_prefix(path)

        # then get the descriptor's json
        return self.ipfs_client.get_json_content(real_path+"/descriptor.json")

    def download_descriptor_resource(self, path, resource):
        # first a standard get in order to retrieve the whole folder
        real_path = self.remove_prefix(path)

        # then get the descriptor's json
        return self.ipfs_client.cat(real_path+"/"+resource)

    def download_docker_image(self, image):
        ipfs_hash = self._image_to_ipfs_hash(image)

        self.ipfs_client.pin(ipfs_hash)

    def delete_docker_image(self, image):
        ipfs_hash = self._image_to_ipfs_hash(image)

        self.ipfs_client.remove_pin(ipfs_hash)

    def download_content(self, content, target, base_folder):
        target = IpfsStorage._remove_leading_slash_if_any(target)

        content = self.remove_prefix(content)

        self.ipfs_client.pin(content)

        intermediate_folder = os.path.dirname(target)

        if intermediate_folder:
            folder_to_create = os.path.join(base_folder, intermediate_folder)

            self.filesystem.makedirs(folder_to_create)

        full_path = self._get_target_full_path(base_folder, target)

        self.filesystem.symlink(content, full_path)

    def remove_content(self, content, target, base_folder):
        target = IpfsStorage._remove_leading_slash_if_any(target)

        content = self.remove_prefix(content)

        self.ipfs_client.remove_pin(content)

        full_path = self._get_target_full_path(base_folder, target)

        self.filesystem.unlink(full_path)

        intermediate_folder = os.path.dirname(target)

        if intermediate_folder:
            folder_to_delete = pathlib.Path(intermediate_folder)

            first_folder_after_base = os.path.join(base_folder, *folder_to_delete.parts[0:1])

            self.filesystem.removedirs(first_folder_after_base)
