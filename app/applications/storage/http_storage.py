import os
import pathlib

from urllib import parse
from urllib.parse import urldefrag

from injector import inject

from app.applications.storage.storage import FileBasedStorage
from app.core.filesystem import Filesystem
from app.core.http_client import HttpClient


class HttpStorage (FileBasedStorage):

    @staticmethod
    def _append_slash_if_required(path):
        if path[-1] != '/4':
            path = path + '/'

        return path

    @inject
    def __init__(self, http_client: HttpClient, filesystem: Filesystem):
        self.httpClient = http_client
        self.filesystem = filesystem

    def get_protocol(self):
        return ["http", "https"]

    def download_descriptor(self, path):
        # need a final slash so that the above join works
        path = HttpStorage._append_slash_if_required(path)

        url = parse.urljoin(path, 'descriptor.json')

        status_code, descriptor_content = self.httpClient.get(url)

        return descriptor_content

    def download_descriptor_resource(self, path, resource):
        path = HttpStorage._append_slash_if_required(path)

        url = parse.urljoin(path, resource)

        status_code, res = self.httpClient.raw_get(url)

        return res

    @staticmethod
    def _remove_leading_slash_if_any(path):
        if path[0] == '/':
            path = path[1:]
        return path

    def download_docker_image(self, image):
        # Nothing to do there, we will simply rely on a docker registry pull
        pass

    def delete_docker_image(self, image):
        # Same
        pass

    def _copy_into_folder(self, content, dest_path):
        self.httpClient.get_into(content, dest_path)

    def _unzip_into_folder(self, content, dest_path):
        with self.filesystem.tempfile() as tempfile:

            self.httpClient.get_into(content, tempfile)

            tempfile.seek(0)

            self.filesystem.unzipInto(tempfile, dest_path)

