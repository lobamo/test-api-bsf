"""Adding language and subject to content and installed_content model.

Revision ID: d7340aaa2cd2
Revises: 5c44316c775b
Create Date: 2020-01-06 11:21:58.944097

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd7340aaa2cd2'
down_revision = '5c44316c775b'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('contents', sa.Column('language', sa.String(length=10), nullable=True))
    op.add_column('contents', sa.Column('subject', sa.String(length=50), nullable=True))
    op.add_column('installed_contents', sa.Column('language', sa.String(length=10), nullable=True))
    op.add_column('installed_contents', sa.Column('subject', sa.String(length=50), nullable=True))


def downgrade():
    op.drop_column('contents', 'language')
    op.drop_column('contents', 'subject')
    op.drop_column('installed_content', 'language')
    op.drop_column('installed_content', 'subject')
